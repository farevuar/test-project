FROM node:8.16-alpine

ARG SSH_PRIVATE_KEY
ARG NPMRC_CONTENT
ARG ENV

ENV NODE_ENV $ENV

ADD package.json /app/
WORKDIR /app

# Add the keys and set permissions, install dependencies, then remove keys
RUN apk add --no-cache --virtual build-dependencies openssh-client git && \

    # Set SSH private key
    mkdir -p /root/.ssh  && \
    chmod 0700 /root/.ssh && \
    echo "$SSH_PRIVATE_KEY" > /root/.ssh/id_rsa && \
    chmod 600 /root/.ssh/id_rsa && \
    ssh-keyscan -p 22247 gitlab.com >> /root/.ssh/known_hosts && \

    # Set npm registry
    echo "$NPMRC_CONTENT" > .npmrc && \
    npm install --no-progress --only=$NODE_ENV && \

    # Clear SSH and NPM tmp files
    rm -f .npmrc && \
    rm -rf /root/.ssh/ && \
    apk del build-dependencies

ADD ./ /app

EXPOSE 3000
CMD [ "npm", "run", "test" ]
